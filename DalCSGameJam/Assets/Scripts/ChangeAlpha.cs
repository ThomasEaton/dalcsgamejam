﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ChangeAlpha : MonoBehaviour
    {

        private MeshRenderer _meshRenderer;

        // Use this for initialization
        void Start ()
        {
            _meshRenderer = GetComponent<MeshRenderer>();

            var color = _meshRenderer.material.color;
            color.a = 0.5f;
            _meshRenderer.material.color = color;

        }
	
        // Update is called once per frame
        void Update () {
		
        }
    }
}
