﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="Alien Model")]
	public class AlienModel : ScriptableObject
	{
		public Sprite Swim1;
		public Sprite Swim2;
	}
}

