﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Quit : MonoBehaviour {

        private float _quitTime = 0;

        private string _quitButton = "Cancel";

        // Use this for initialization
        void Start () {
		
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (Input.GetButton(_quitButton))
            {
                _quitTime += Time.deltaTime;
            }
            else
            {
                _quitTime = 0;
            }

            if (_quitTime > 2f)
            {
                Application.Quit();
                _quitTime = 0;
            }
        }
    }
}
