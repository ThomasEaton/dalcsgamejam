﻿using UnityEngine;

namespace Assets.Scripts
{
    public class SkyBuddyController : MonoBehaviour
    {

        public Transform FollowTarget;

        public float Offset;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (FollowTarget != null)
            {
                var pos = transform.position;
                pos.x = FollowTarget.position.x + Offset;
                transform.position = pos;
            }
        }
    }
}
