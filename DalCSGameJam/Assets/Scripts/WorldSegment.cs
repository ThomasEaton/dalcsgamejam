﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshCollider))]
    public class WorldSegment : MonoBehaviour {

        private MeshFilter _meshFilter;
        private MeshCollider _meshCollider;

        // Use this for initialization
        void Start () {
		
        }
	
        // Update is called once per frame
        void Update () {
		
        }

        public void SetMesh(Mesh mesh) {
            _meshFilter = GetComponent<MeshFilter> ();
            _meshCollider = GetComponent<MeshCollider> ();

            _meshFilter.mesh = mesh;
            _meshCollider.sharedMesh = mesh;
        }
    }
}
