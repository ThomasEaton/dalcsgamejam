﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ShipController : MonoBehaviour {

		[Range (0, 1)]
        public float ShipSpeed = 0.5f;
		[Range (0, 1)]
        public float RotationSpeed = 0.5f;

		public float SpeedLimit = 2f;
        private Rigidbody _rigidBody;

		[HideInInspector]
        public Vector3 LeftSide = new Vector3(0, 0, 0);
		[HideInInspector]
        public Vector3 RightSide = new Vector3(0, 180, 0);

		public string ShipAxis = "ShipHorizontal";

        [HideInInspector]
        public bool rotating = false;
		[HideInInspector]
		public Vector3 RotationDirection;

        void Start() {
            _rigidBody = this.GetComponent<Rigidbody> ();
        }

        // Update is called once per frame
        void Update () {
            Sail ();
			//Flip ();
        }

		//private void Flip(){
		//	if (rotating)
		//	{
		//		if (Vector3.Distance(transform.eulerAngles, RotationDirection) > 0.01f)
		//	    {
		//			transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, RotationDirection, Time.deltaTime * RotationSpeed);
		//	    }
		//	    else
		//	    {
		//			transform.eulerAngles = RotationDirection;
		//	        rotating = false;
		//	    }
		//	}
		//}

        private void Sail(){
			float direction = Input.GetAxis(ShipAxis);
			if (_rigidBody.velocity.magnitude < SpeedLimit) {
				_rigidBody.velocity += Vector3.right * direction * ShipSpeed;
			}

            //Left
			if (Input.GetAxis(ShipAxis) < 0){
                rotating = true;
				RotationDirection = LeftSide;
                if (_rigidBody.velocity.x > 0) {
                    _rigidBody.velocity = new Vector2(_rigidBody.velocity.x / (1f + Time.deltaTime), _rigidBody.velocity.y);
                }
            }
            //Right
			else if (Input.GetAxis(ShipAxis) > 0){
                rotating = true;
				RotationDirection = RightSide;
                if (_rigidBody.velocity.x < 0) {
                    _rigidBody.velocity = new Vector2(_rigidBody.velocity.x / (1f + Time.deltaTime), _rigidBody.velocity.y);
                }
            }
            //Decelerate
            else{
                _rigidBody.velocity = new Vector2(_rigidBody.velocity.x / (1f + Time.deltaTime), _rigidBody.velocity.y);
            }
	
        }

    }
}
