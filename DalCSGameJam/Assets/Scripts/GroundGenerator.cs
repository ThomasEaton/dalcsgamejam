﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class GroundGenerator : MonoBehaviour
    {
        public WorldSegment WorldSegmentPrefab;

		private List<Transform> DoodadsList = new List<Transform>();

        public int TileWidth;
        public int TileHeight;

        public int TileSheetWidth;
        public int TileSheetHeight;

        public float WorldTileWidth = 100f;
        public float WorldTileHeight = 100f;

        private List<Vector3> vertices;
        private List<int> triangles;
        private List<Vector2> uvs;

        private int triangleIndex;

        // Use this for initialization
        void Start()
        {

            //        StartMesh ();
            //        EndMesh ();

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartMesh()
        {

            vertices = new List<Vector3>();
            triangles = new List<int>();

            uvs = new List<Vector2>();

            triangleIndex = 0;
        }

		public GameObject EndMesh(string Path)
        {
            var mesh = new Mesh();

            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
            mesh.SetUVs(0, uvs);

            var worldSegment = Instantiate(WorldSegmentPrefab, this.transform.position, Quaternion.identity);

			for (var i = 0; i < DoodadsList.Count; i++) {
				DoodadsList [i].parent = worldSegment.transform;
			}

			DoodadsList.Clear ();

            worldSegment.SetMesh(mesh);

			return worldSegment.gameObject;
        }

        public void AddTile(int WorldX, int WorldY, int TileX, int TileY)
        {

            WorldY = -WorldY;

            var worldStartX = WorldX * (WorldTileWidth / 100f);
            var worldStartY = WorldY * (WorldTileHeight / 100f);

            var worldEndX = worldStartX + (WorldTileWidth / 100f);
            var worldEndY = worldStartY + (WorldTileHeight / 100f);



            vertices.AddRange(new Vector3[] {
                // Side
                new Vector3(worldStartX, worldEndY, -1f),
                new Vector3(worldEndX, worldEndY, -1f),
                new Vector3(worldEndX, worldStartY, -1f),
                new Vector3(worldStartX, worldStartY, -1f),

                // Top
                new Vector3(worldStartX, worldEndY, -1f),
                new Vector3(worldStartX, worldEndY, 1f),
                new Vector3(worldEndX, worldEndY, 1f),
                new Vector3(worldEndX, worldEndY, -1f),

                // Bottom
                new Vector3(worldStartX, worldStartY, -1f),
                new Vector3(worldEndX, worldStartY, -1f),
                new Vector3(worldEndX, worldStartY, 1f),
                new Vector3(worldStartX, worldStartY, 1f),

                // Left
                new Vector3(worldStartX, worldEndY, 1f),
                new Vector3(worldStartX, worldEndY, -1f),
                new Vector3(worldStartX, worldStartY, -1f),
                new Vector3(worldStartX, worldStartY, 1f),

//            // Right (Reversed)
                new Vector3(worldEndX, worldEndY, 1f),
                new Vector3(worldEndX, worldStartY, 1f),
                new Vector3(worldEndX, worldStartY, -1f),
                new Vector3(worldEndX, worldEndY, -1f),

                // Back
                new Vector3(worldStartX, worldEndY, 1f),
                new Vector3(worldEndX, worldEndY, 1f),
                new Vector3(worldEndX, worldStartY, 1f),
                new Vector3(worldStartX, worldStartY, 1f),
            });

            triangles.AddRange(new int[] {
                triangleIndex+0, triangleIndex+1, triangleIndex+3,
                triangleIndex+1, triangleIndex+2, triangleIndex+3,

                triangleIndex+4, triangleIndex+5, triangleIndex+7,
                triangleIndex+5, triangleIndex+6, triangleIndex+7,

                triangleIndex+8, triangleIndex+9, triangleIndex+11,
                triangleIndex+9, triangleIndex+10, triangleIndex+11,

                triangleIndex+12, triangleIndex+13, triangleIndex+15,
                triangleIndex+13, triangleIndex+14, triangleIndex+15,

                triangleIndex+16, triangleIndex+17, triangleIndex+19,
                triangleIndex+17, triangleIndex+18, triangleIndex+19,

                triangleIndex+20, triangleIndex+21, triangleIndex+23,
                triangleIndex+21, triangleIndex+22, triangleIndex+23,
            });

            triangleIndex += 4 * 6;

            var startX = (TileWidth * TileX) / (float)TileSheetWidth;
            var endX = (TileWidth * TileX) / (float)TileSheetWidth + TileWidth / (float)TileSheetWidth;

            var startY = (TileHeight * TileY) / (float)TileSheetHeight;
            var endY = (TileHeight * TileY) / (float)TileSheetHeight + TileHeight / (float)TileSheetHeight;


            uvs.AddRange(new Vector2[] {
                new Vector2(startX + 0f, 1f-startY),
                new Vector2(endX, 1f-startY),
                new Vector2(endX, 1f-endY),
                new Vector2(startX, 1f-endY),

                Vector2.zero,
                Vector2.zero,
                Vector2.zero,
                Vector2.zero,

                Vector2.zero,
                Vector2.zero,
                Vector2.zero,
                Vector2.zero,

                Vector2.zero,
                Vector2.zero,
                Vector2.zero,
                Vector2.zero,

                Vector2.zero,
                Vector2.zero,
                Vector2.zero,
                Vector2.zero,

                Vector2.zero,
                Vector2.zero,
                Vector2.zero,
                Vector2.zero,
            });
        }

        public void AddDoodad(int WorldX, int WorldY, Transform Doodad)
        {

            WorldY = -WorldY;

            var position = new Vector3(WorldX * (WorldTileWidth / 100f) + (WorldTileWidth / 100f) / 2, WorldY * (WorldTileHeight / 100f) + (WorldTileHeight / 100f) / 2, 0.25f);

            var doodad = Instantiate(Doodad, position, Quaternion.identity);
			DoodadsList.Add (doodad);
        }
    }
}