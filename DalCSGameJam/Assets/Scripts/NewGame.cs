﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Submit")) {

			var sessionManager = GameObject.FindObjectOfType<SessionController> ();

			if (sessionManager != null) {
				Destroy (sessionManager);
			}

			SceneManager.LoadScene (0);
		}
	}
}