﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="GroundTileSet", fileName="GroundTileSet")]
	public class GroundTileset : ScriptableObject
	{
		public Tile BaseGround;
		public Tile NoBottomLeft;
		public Tile NoBottomRight;
		public Tile NoBottomBoth;

		public Tile Surrounded;

		public Tile UpLeft;
		public Tile UpRight;

		[Serializable]
		public class Tile {
			public int X;
			public int Y;
		}
	}
}

