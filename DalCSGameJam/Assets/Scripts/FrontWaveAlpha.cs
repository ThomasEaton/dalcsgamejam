﻿using UnityEngine;

namespace Assets
{
    public class FrontWaveAlpha : MonoBehaviour
    {

        private SpriteRenderer _spriteRenderer;

        // Use this for initialization
        void Start ()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            var color = _spriteRenderer.color;
            color.a = 0.5f;
            _spriteRenderer.color = color;
        }
	
        // Update is called once per frame
        void Update ()
        {
	    	
        }
    }
}
