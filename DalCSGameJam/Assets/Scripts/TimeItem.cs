﻿using System;
using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="Collectable/TimeItem")]
	public class TimeItem : CollectableItem
	{
		public int TimeBonus;

		public AudioClip TimeAudio;
		public float ClipDuration = 0.2f;

		public bool MultipleCount = true;

		public override IEnumerator OnCollectItem (GameController gameManager)
		{
			if (MultipleCount) {

				for (var i = 0; i < TimeBonus; i++) {
					gameManager.Timer += 1;

					gameManager.AudioSource.PlayOneShot (TimeAudio);

					yield return new WaitForSeconds (ClipDuration);
				}
			} else {
				gameManager.Timer += TimeBonus;

				gameManager.AudioSource.PlayOneShot (TimeAudio);

				yield return new WaitForSeconds (ClipDuration);
			}
		}
	}
}

