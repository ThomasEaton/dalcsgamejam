﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Bob : MonoBehaviour {

        public float BobSpeed;
        [Range (0f, 1f)]
        public float BobTime = 1f;

        [Range (0f, 1f)]
        public float BobClamp = 1f;
        // Use this for initialization
        void Start () {
		
        }
	
        // Update is called once per frame
        void Update () {
            transform.Translate(Vector2.up * BobSpeed * Time.deltaTime);

            BobSpeed += Time.deltaTime * BobTime;

            if (BobSpeed >= BobClamp)
            {
                BobSpeed = -BobClamp;
            }
        }
    }
}
