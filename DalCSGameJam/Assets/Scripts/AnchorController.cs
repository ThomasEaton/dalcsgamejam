﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;

[RequireComponent(typeof(SpriteRenderer))]
public class AnchorController : MonoBehaviour {

	public Sprite AnchorSprite;
	public CollectableItem CollectableItem;

	private SpriteRenderer _spriteRenderer;

	// Use this for initialization
	void Start () {
		_spriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (CollectableItem == null) {
			_spriteRenderer.sprite = AnchorSprite;
		} else {
			_spriteRenderer.sprite = CollectableItem.ItemOnAnchorSprite;
		}
	}
}
