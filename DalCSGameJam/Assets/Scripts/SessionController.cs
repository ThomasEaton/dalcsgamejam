﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;

public class SessionController : MonoBehaviour {

	public AlienModel SelectedAlien;
	public List<AlienModel> AliensList;

	// Use this for initialization
	void Start () {
		if (SelectedAlien == null) {
			SelectedAlien = AliensList [0];
		}
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
