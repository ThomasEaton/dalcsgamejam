﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssemblyCSharp;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(GroundGenerator))]
    public class GroundFileParser : MonoBehaviour {

        public GroundTileset GroundTileset;
        public TextAsset GroundFileName;
        public TextAsset DoodadFileName;
        
        public List<Doodad> Doodads;

        [Serializable]
        public class Doodad
        {
            public int Id;
            public Transform Prefab;
        }

        // Use this for initialization
        void Start () {
		
        }
	
        // Update is called once per frame
        void Update () {
		
        }

		public GameObject ProcessGroundSegment(List<List<int>> groundSegmentList, string FileName, bool endMesh = true) {

            var maxWidth = 0;
            foreach (var levelList in groundSegmentList) {
                maxWidth = Mathf.Max(levelList.Count, maxWidth);
            }

            var groundGrid = new int [maxWidth, groundSegmentList.Count];
            for (var y = 0; y < groundSegmentList.Count; y++) {
                for (var x = 0; x < groundSegmentList [y].Count; x++) {
                    groundGrid [x, y] = groundSegmentList [y] [x];
                }
            }

            var generator = GetComponent<GroundGenerator> ();

            generator.StartMesh ();

            for (var y = 0; y < groundGrid.GetLength(1); y++) {
                for (var x = 0; x < groundGrid.GetLength(0); x++) {

                    var tile = GetTile (x, y, groundGrid);

                    if (tile != null) {
                        generator.AddTile (x, y, tile.X, tile.Y);
                    }
                }
            }

			if (endMesh) {
				return generator.EndMesh (GroundFileName.name);
			}

			return null;

            
        }

        public AssemblyCSharp.GroundTileset.Tile GetTile(int x, int y, int[,] groundGrid) {

            var isGround = IsGround (x, y, groundGrid);

            if (isGround) {
                var isSurrounded = IsGround (x - 1, y, groundGrid) && IsGround (x, y - 1, groundGrid) && IsGround (x + 1, y, groundGrid) && IsGround (x, y + 1, groundGrid);

                if (isSurrounded)
                    return GroundTileset.Surrounded;

                var isTop = !IsGround (x, y - 1, groundGrid);

                if (isTop) {

                    var hasBottom = IsGround (x, y + 1, groundGrid);

                    var hasLeft = IsGround (x - 1, y, groundGrid);
                    var hasRight = IsGround(x+1,y, groundGrid);

                    if ((hasLeft && hasRight) || hasBottom)
                        return GroundTileset.BaseGround;
                    if (hasLeft && !hasRight && !hasBottom)
                        return GroundTileset.NoBottomRight;
                    if (!hasLeft && hasRight & !hasBottom)
                        return GroundTileset.NoBottomLeft;

                    return GroundTileset.NoBottomBoth;
                } else {
                    return GroundTileset.Surrounded;
                }

            } else {

                return null;
            }
        }

        public bool IsGround(int x, int y, int [,] groundGrid) {
            if (x < 0)
                return false;
            if (y < 0)
                return false;
            if (x >= groundGrid.GetLength (0))
                return false;
            if (y >= groundGrid.GetLength (1))
                return false;

            return groundGrid [x, y] == 1;
        }

		public GameObject ProcessDoodadSegment(List<List<int>> doodadSegmentList, bool endMesh = false) {
            var maxWidth = 0;
            foreach (var levelList in doodadSegmentList) {
                maxWidth = Mathf.Max(levelList.Count, maxWidth);
            }

            var doodadGrid = new int [maxWidth, doodadSegmentList.Count];
            for (var y = 0; y < doodadSegmentList.Count; y++) {
                for (var x = 0; x < doodadSegmentList [y].Count; x++) {

                    doodadGrid [x, y] = doodadSegmentList [y] [x];
                }
            }

            var generator = GetComponent<GroundGenerator> ();

            for (var y = 0; y < doodadSegmentList.Count; y++) {
                for (var x = 0; x < doodadSegmentList [y].Count; x++) {
				
                    var doodad = Doodads.FirstOrDefault(d => d.Id == doodadSegmentList [y] [x]);

                    if (doodad != null) {
                        generator.AddDoodad (x, y, doodad.Prefab);
                    }
                }
            }

			if (endMesh) {
				return generator.EndMesh (GroundFileName.name);
			}

			return null;

        }
    }
}