﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour {


	public Transform parent;
	public float Distance = 3.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		var treasures = GameObject.FindObjectsOfType<AnchorableItem> ();

		var distance = float.MaxValue;
		AnchorableItem closest = null;
		for (var i = 0; i < treasures.Length; i++) {
			var compare = (treasures [i].transform.position - parent.transform.position).magnitude;
			if (compare < distance && treasures[i].ShowsUpAsArrow) {
				closest = treasures [i];
				distance = compare;
			}
		}

		var anchorPosition = closest.transform.position;
		anchorPosition.z = 0;
		var playerPosition = parent.transform.position;
		playerPosition.z = 0;

		var arrowPosition = (anchorPosition - playerPosition).normalized * Distance;
		arrowPosition.z = -3;

		this.transform.localPosition = arrowPosition;
	}
}
