﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class FallInAnimation : MonoBehaviour {

	public float StartOffset = 0.5f;
	public AnimationCurve FallDownAnimation;
	public float FallInHeight = 10f;

	public float FallInTime = 0.5f;

	private Vector3 EndPosition;

	private float CurrentTime = 0f;

	private bool IsFinished = false;

	// Use this for initialization
	void Start () {
		EndPosition = this.transform.position;
		this.transform.Translate(Vector3.up*FallInHeight);
	}
	
	// Update is called once per frame
	void Update () {

		CurrentTime += Time.deltaTime;
		var FallingTime = CurrentTime - StartOffset;

		var currentTime = Mathf.Clamp01 (FallingTime / FallInTime);

		var pos = this.transform.position;

		var yDiff = FallInHeight - EndPosition.y;

		var y = (FallDownAnimation.Evaluate (currentTime) - 1) * yDiff;

		pos.y = y + EndPosition.y;

		this.transform.position = pos;

		if (currentTime > 0.98 && IsFinished == false) {
			IsFinished = true;
			this.GetComponent<AudioSource> ().Play ();
		}


	}
}
