﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Editor
{
    [CustomEditor(typeof(GroundFileParser))]
    public class GroundFileParserEditor : UnityEditor.Editor {
        
		[SerializeField]
		private int GenerateToolbarSelection = 0;

		public string DatabaseFolder = "Assets/Segments/";

        public override void OnInspectorGUI() {

            DrawDefaultInspector ();

			DatabaseFolder = GUILayout.TextField (DatabaseFolder);

            var parser = target as GroundFileParser;

            GenerateToolbarSelection = GUILayout.Toolbar (GenerateToolbarSelection, new string[] { "Both", "Mesh", "Doodad" });

            if (GenerateToolbarSelection == 0)
            if (GUILayout.Button("Generate Mesh and Doodads")) {
                var fileContents = Load (parser.GroundFileName);
				parser.ProcessGroundSegment (fileContents, parser.GroundFileName.name, false);

                var doodadContents = Load (parser.DoodadFileName);
				var worldSegment =  parser.ProcessDoodadSegment (doodadContents, true);

				var mesh = worldSegment.GetComponent<MeshFilter> ().sharedMesh;
				AssetDatabase.CreateAsset (mesh, DatabaseFolder + parser.GroundFileName.name + "Mesh.asset");
				AssetDatabase.LoadAssetAtPath<Mesh> (AssetDatabase.GetAssetPath (mesh));
				AssetDatabase.Refresh ();

				PrefabUtility.CreatePrefab (DatabaseFolder + parser.GroundFileName.name + ".prefab", worldSegment, ReplacePrefabOptions.ReplaceNameBased);
            }

            if (GenerateToolbarSelection == 1)
            if (GUILayout.Button("Generate Mesh")) {
                var fileContents = Load (parser.GroundFileName);
				var worldSegment = parser.ProcessGroundSegment (fileContents, parser.GroundFileName.name);

				var mesh = worldSegment.GetComponent<MeshFilter> ().mesh;
				AssetDatabase.CreateAsset (mesh, "Assets/" + parser.GroundFileName.name + "Mesh.asset");

				PrefabUtility.CreatePrefab ("Assets/" + parser.GroundFileName.name + ".prefab", worldSegment, ReplacePrefabOptions.ReplaceNameBased);
            }

            if (GenerateToolbarSelection == 2)
            if (GUILayout.Button("Generate Doodads")) {
                var fileContents = Load (parser.DoodadFileName);

                parser.ProcessDoodadSegment (fileContents);
            }
        }

        private List<List<int>> Load(TextAsset fileName)
        {
			var lines = fileName.text.Split (new string [] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            // Handle any problems that might arise when reading the text
            try
            {
                string line;
                var lineListList = new List<List<int>>();

                for (var i=0;i<lines.Length;i++) {
                    line = lines[i];

                    var lineList = new List<int>();

                    if (line != null)
                    {
                        string[] entries = line.Split(',');
                        if (entries.Length > 0) {
                            var stringList = entries.ToList();

                            foreach(var entry in stringList) {
                                lineList.Add(int.Parse(entry));
                            }
                        }
                    }

                    lineListList.Add(lineList);
                }

                return lineListList;
            }
                // If anything broke in the try block, we throw an exception with information
                // on what didn't work
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return null;
            }
        }

    }
}


