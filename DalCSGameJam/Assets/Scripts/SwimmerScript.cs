﻿using System.Collections;
using UnityEngine;
using AssemblyCSharp;

namespace Assets.Scripts
{
	[RequireComponent (typeof(Rigidbody))]
    public class SwimmerScript : MonoBehaviour {

        public float BobSpeed = 0f;
        public float SwimSpeed = 200f;
        public float SwimDownSpeed = 60f;
        public float FloatSpeed = 40f;
        public float MaxVelocity = 5f;

        public float WaterLevel = 0f;

		private AlienModel _currentAlien;
		public AlienModel DefaultAlien;

        public Sprite SpriteSwimOne;
        public Sprite SpriteSwimTwo;

	    public string SwimmerAxis = "SwimmerHorizontal";
	    public string SwimButton = "SwimButton";

		private int _swimState = 0;
        
        private Rigidbody _rigidBody;
        private SpriteRenderer _spriteRenderer;

        // Use this for initialization
        void Start ()
        {
            _rigidBody = GetComponent<Rigidbody>();
            _spriteRenderer = GetComponent<SpriteRenderer>();

			var sessionController = GameObject.FindObjectOfType<SessionController> ();

			if (sessionController == null) {
				_currentAlien = DefaultAlien;
			} else {
				_currentAlien = sessionController.SelectedAlien;
				_spriteRenderer.sprite = _currentAlien.Swim1;
			}

			if (_spriteRenderer != null) {
				StartCoroutine (SwimAnimation ());
			}
        }
	
        // Update is called once per frame
        void Update ()
        {
            if (_rigidBody.position.y > WaterLevel + 1)
            {
                transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -100000f, WaterLevel + 1f), transform.position.z);
                _rigidBody.velocity = new Vector3(_rigidBody.velocity.x, 0, _rigidBody.velocity.z);
            }
            //Bob Animation when on water surface
            if (_rigidBody.position.y < WaterLevel - 1)
            {
                FloatUp();
            }
            else
            {
                Bob();
            }

            //Left and Right movement
            HorizontalSwimming();

            //Swim down
            SwimDown();
        }

        private void HorizontalSwimming()
        {
            //Left
            _rigidBody.AddForce(Input.GetAxis(SwimmerAxis) * Vector3.right * SwimSpeed * Time.deltaTime);
            if (Input.GetAxis(SwimmerAxis) < 0)
            {
                _spriteRenderer.flipX = true;
                if (_rigidBody.velocity.x > 0)
                {
                    _rigidBody.velocity = new Vector3(-3, _rigidBody.velocity.y, 0);
                }
            }
            //Right
            else if (Input.GetAxis(SwimmerAxis) > 0)
            {
                _spriteRenderer.flipX = false;
                if (_rigidBody.velocity.x < 0)
                {
                    _rigidBody.velocity = new Vector3(3, _rigidBody.velocity.y, 0);
                }
            }
            //Decelerate
            else
            {
                _rigidBody.velocity = new Vector3(_rigidBody.velocity.x / (1f + Time.deltaTime), _rigidBody.velocity.y, 0);
            }
        }

        private void SwimDown()
        {
            if (Input.GetButtonDown(SwimButton))
            {
                _rigidBody.AddForce(Vector3.down * SwimDownSpeed);

                if (_rigidBody.velocity.sqrMagnitude > MaxVelocity)
                {
                    //smoothness of the slowdown is controlled by the 0.99f, 
                    //0.5f is less smooth, 0.9999f is more smooth
                    _rigidBody.velocity *= 0.99f;
                }
            }
        }

        public void FloatUp()
        {
            _rigidBody.AddForce(Vector3.up * FloatSpeed * Time.deltaTime);
        }

        //Bob Animation when on water surface
        private void Bob()
        {
            transform.Translate(Vector3.up * BobSpeed * Time.deltaTime);

            BobSpeed += Time.deltaTime;

            if (BobSpeed >= 1f)
            {
                BobSpeed = -1f;
            }
        }

        public IEnumerator SwimAnimation()
        {
            yield return new WaitForSeconds(0.25f);
            
			if (_swimState == 0)
            {
				_spriteRenderer.sprite = _currentAlien.Swim1;
				_swimState = 1;
            }
			else if (_swimState == 1)
            {
				_spriteRenderer.sprite = _currentAlien.Swim2;
				_swimState = 0;
            }

            StartCoroutine(SwimAnimation());
        }
    }
}
