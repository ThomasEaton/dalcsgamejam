﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class GameController : MonoBehaviour {

	public int Score = 0;
	public float Timer = 120f;

	[HideInInspector] public AudioSource AudioSource;

	public AudioClip GameOverSound;
	public float GameOverTime = 1.25f;
	private bool IsGameOver;

	// Use this for initialization
	void Start () {
		AudioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (!IsGameOver) {
			Timer -= Time.deltaTime;

			if (Timer < 0f) {
				IsGameOver = true;
				AudioSource.PlayOneShot(GameOverSound);
				Time.timeScale = 0.2f;

				Timer = 0f;

				StartCoroutine (RestartGame ());
			}
		}
	}

	public IEnumerator RestartGame() {
		yield return new WaitForSeconds (GameOverTime * Time.timeScale);

		Time.timeScale = 1f;

		SceneManager.LoadScene (2);
	}
}
