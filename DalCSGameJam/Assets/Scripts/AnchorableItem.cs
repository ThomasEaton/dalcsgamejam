﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;

public class AnchorableItem : MonoBehaviour {

	public CollectableItem CollectableItem;
	public bool ShowsUpAsArrow = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision coll) {

		var anchor = coll.gameObject.GetComponent<AnchorController> ();
		if (anchor != null) {
			if (anchor.CollectableItem == null) {
				anchor.CollectableItem = this.CollectableItem;
				Destroy (this.gameObject);
			}
		}
	}
}
