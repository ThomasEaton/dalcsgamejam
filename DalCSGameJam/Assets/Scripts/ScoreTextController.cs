﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreTextController : MonoBehaviour {

	private GameController _gameController;

	private Text _text;

	// Use this for initialization
	void Start () {
		_text = GetComponent<Text> ();
		_gameController = GameObject.FindObjectOfType<GameController> ();
	}

	// Update is called once per frame
	void Update () {
		_text.text = "Score: " + _gameController.Score;
	}
}
