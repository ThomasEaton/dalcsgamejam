﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class ChainSegmentSpawner : MonoBehaviour {

		private GameController _gameController;

        public ChainSegment ChainSegmentPrefab;
        public ChainSegment AnchorSegmentPrefab;

        public Sprite ChainSprite;
        public Sprite AnchorSprite;

        public string ChainLayer;
        public string AnchorLayer;

        private ChainSegment _firstSegment;
        private ChainSegment _currentTopSegment;

        private readonly List<ChainSegment> _chainSegments = new List<ChainSegment>();

        public string ChainAxis = "ChainAxis";

        private float _totalLength = 0f;

        public float MaxDistance = 1.25f;
        public float ChainLowerTime = 0.5f;

        public bool IsBlocked = false;

		// Use this for initialization
		void Start () {
			_gameController = GameObject.FindObjectOfType<GameController> ();
		}
	
        // Update is called once per frame
        void Update () {

            _totalLength += -Input.GetAxis (ChainAxis) * Time.deltaTime;
            if (Mathf.Abs(Input.GetAxis (ChainAxis)) < 0.1) {
                _totalLength = 0;
            }

            //Debug.Log (_totalLength);

            // Lower the Chain
            if (_totalLength >= ChainLowerTime || _firstSegment == null) {

                if (!IsBlocked) {

                    _totalLength -= ChainLowerTime;

                    ChainSegment newTop;
                    if (_firstSegment != null) {
					
                        if (_chainSegments.Count <= 1) {
                            newTop = Instantiate (ChainSegmentPrefab, this.transform.position + Vector3.down * 1f, _currentTopSegment.transform.rotation);
                        } else {
                            newTop = Instantiate (_currentTopSegment, this.transform.position + Vector3.down * 1f, _currentTopSegment.transform.rotation);
                        }

                    } else {
                        newTop = Instantiate (AnchorSegmentPrefab, this.transform.position+Vector3.down*0.5f, Quaternion.Euler(this.transform.rotation.eulerAngles));
                    }
					
                    if (_currentTopSegment != null) {
                        _currentTopSegment.GetComponent<HingeJoint>().connectedBody = newTop.GetComponent<Rigidbody> ();
                    }

                    if (_firstSegment == null) {
                        _firstSegment = newTop;
                        _firstSegment.name = "FirstSegment";
                    }

                    _chainSegments.Add (newTop);
                    _currentTopSegment = newTop;

                    newTop.GetComponent<HingeJoint>().connectedBody = this.GetComponent<Rigidbody>();
                }
            }

            if (_totalLength < -ChainLowerTime) {

                _totalLength += ChainLowerTime;

                if (_currentTopSegment != _firstSegment) {
				
                    var hinge = _firstSegment.GetComponent<HingeJoint> ();
                    var parent = hinge.connectedBody.GetComponent<HingeJoint> ();

                    if (parent.connectedBody != null && parent.connectedBody != this) {

                        var parent2 = parent.connectedBody.GetComponent<Rigidbody> ();

                        hinge.transform.position = parent.transform.position;
                        hinge.transform.rotation = parent.transform.rotation;

                        hinge.connectedBody = parent2;

                        var parentChain = parent.GetComponent<ChainSegment> ();
                        _chainSegments.Remove (parentChain);

                        if (_currentTopSegment == parentChain) {
                            _currentTopSegment = _firstSegment;
                        }

                        Destroy (parent.gameObject);
                    }
                } 
                // You've retracted all of the way, collect the item!
                else {
                    var anchor = _firstSegment.GetComponent<AnchorController> ();
                    if (anchor != null && anchor.CollectableItem != null) {

                        var item = anchor.CollectableItem;
                        anchor.CollectableItem = null;

						StartCoroutine (item.OnCollectItem (_gameController));
					}
				}
			}

		// Debug.Log ("Total: " + TotalLength);
		// CurrentTopSegment.connectedBody
		}
	}
}
