﻿using System;
using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="Collectable/ScoreItem")]
	public class ScoreItem : CollectableItem
	{
		public int Score = 10;

		public AudioClip ScoreAudio;
		public float ClipDuration = 0.2f;

		public bool MultipleCount = true;
		public int ScoreMultiple = 1;

		public override IEnumerator OnCollectItem(GameController gameManager) {

			if (MultipleCount) {
				
				for (var i = 0; i < Score; i+=ScoreMultiple) {
					gameManager.Score += ScoreMultiple;

					gameManager.AudioSource.PlayOneShot (ScoreAudio);

					yield return new WaitForSeconds (ClipDuration);
				}
			} else {
				gameManager.Score += Score;

				gameManager.AudioSource.PlayOneShot (ScoreAudio);

				yield return new WaitForSeconds (ClipDuration);
			}
		}
	}
}

