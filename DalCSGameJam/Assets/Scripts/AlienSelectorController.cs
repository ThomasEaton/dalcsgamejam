﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlienSelectorController : MonoBehaviour {

	private SessionController _sessionController;

	public List<Image> AlienImages;

	// Use this for initialization
	void Start () {
		_sessionController = GameObject.FindObjectOfType<SessionController> ();

		_sessionController.SelectedAlien = _sessionController.AliensList [Random.Range (0, _sessionController.AliensList.Count)];
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("SwimmerHorizontal")) {
			var direction = (int)Mathf.Sign (Input.GetAxis ("SwimmerHorizontal"));

			var s = _sessionController.SelectedAlien;
			var ind = _sessionController.AliensList.IndexOf (s);

			ind += direction;
			ind %= _sessionController.AliensList.Count;

			if (ind == -1) {
				ind = _sessionController.AliensList.Count - 1;
			}
		
			_sessionController.SelectedAlien = _sessionController.AliensList [ind];
		}



		for (var i = 0; i < AlienImages.Count; i++) {
			var color = AlienImages [i].color;
			color.a = 0.5f;
			AlienImages [i].color = color;

			AlienImages [i].sprite = _sessionController.AliensList [i].Swim1;
		}

		var selected = _sessionController.SelectedAlien;

		var index = _sessionController.AliensList.IndexOf (selected);

		var c = AlienImages [index].color;
		c.a = 1f;
		AlienImages [index].color = c;
	}
}
