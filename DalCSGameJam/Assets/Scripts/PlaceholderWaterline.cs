﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderWaterline : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawLine (new Vector3 (-100, 0, 0), new Vector3 (100, 0, 0));
	}
}
