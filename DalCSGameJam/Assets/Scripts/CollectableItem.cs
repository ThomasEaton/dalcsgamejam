﻿using System;
using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	public abstract class CollectableItem : ScriptableObject
	{
		public Sprite ItemOnAnchorSprite;

		public abstract IEnumerator OnCollectItem(GameController gameManager);
	}
}

