﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishScript : MonoBehaviour
{
    public float SwimSpeed = 5f;
    private bool left = true;

    public Sprite SpriteSwimLeftOne;
    public Sprite SpriteSwimLeftTwo;
    public Sprite SpriteDead;

    //private Rigidbody _rigidBody;
    private SpriteRenderer _spriteRenderer;

	// Use this for initialization
	void Start ()
	{
	    //_rigidBody = GetComponent<Rigidbody>();
	    _spriteRenderer = GetComponent<SpriteRenderer>();

	    StartCoroutine(SwimAnimation());
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (left)
	    {
	        transform.Translate(Vector3.left * SwimSpeed * Time.deltaTime);
	        _spriteRenderer.flipX = false;
	    }
	    else
	    {
            transform.Translate(Vector3.right * SwimSpeed * Time.deltaTime);
	        _spriteRenderer.flipX = true;
	    }


    }

    public IEnumerator SwimAnimation()
    {
        yield return new WaitForSeconds(0.25f);

        if (_spriteRenderer.sprite == SpriteSwimLeftOne)
        {
            _spriteRenderer.sprite = SpriteSwimLeftTwo;
        }
        else if (_spriteRenderer.sprite == SpriteSwimLeftTwo)
        {
            _spriteRenderer.sprite = SpriteSwimLeftOne;
        }

        StartCoroutine(SwimAnimation());
    }

    void OnCollisionEnter(Collision collision)
    {
        left = !left;
    }
    
}
